package ru.tsc.kyurinova.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getPasswordSecret();

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getDeveloperName();

    @NotNull
    String getDeveloperEmail();

    @NotNull String getSessionSecret();

    @NotNull Integer getSessionIteration();

    @NotNull String getServerHost();

    @NotNull String getServerPort();

    @NotNull String getJdbcUser();

    @NotNull String getJdbcPassword();

    @NotNull String getJdbcUrl();

    @NotNull String getJdbcDriver();
}
