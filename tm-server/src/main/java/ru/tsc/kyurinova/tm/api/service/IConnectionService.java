package ru.tsc.kyurinova.tm.api.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;

public interface IConnectionService {

    @NotNull SqlSession getSqlSession();

    @NotNull
    @SneakyThrows
    SqlSessionFactory getSqlSessionFactory();
}
