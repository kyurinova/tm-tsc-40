package ru.tsc.kyurinova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.command.AbstractCommand;
import ru.tsc.kyurinova.tm.endpoint.Session;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "change-password";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "change user's password...";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSessionService().getSession();
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();
        serviceLocator.getAdminUserEndpoint().setPasswordUser(session, session.getUserId(), password);
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
